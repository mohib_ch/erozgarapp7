import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
} from "react-native";
import React from "react";
import { Header } from "../components/header";

const Login = () => {
  return (
    <View style={styles.container}>
      <Header title={"Login"} />
      <View style={styles.form}>
        <TextInput style={styles.input} placeholder="email" />
        <TextInput style={styles.input} placeholder="password" />

        <TouchableOpacity style={styles.button}>
          <Text style={styles.buttonText}>Login</Text>
        </TouchableOpacity>
      </View>
      <View style={styles.blank}></View>
    </View>
  );
};

export default Login;

const styles = StyleSheet.create({
  container: {
    flex: 3,
    backgroundColor: "#fff8ff",
  },
  form: {
    flex: 1.5,
    padding: 10,
  },
  input: {
    backgroundColor: "white",
    padding: 15,
    marginVertical: 10,
  },
  button: {
    width: "90%",
    padding: 10,
    backgroundColor: "red",
    borderRadius: 20,
    alignItems: "center",
    alignSelf: "center",
  },
  buttonText: {
    color: "white",
  },
  blank: {
    flex: 1,
  },
});
