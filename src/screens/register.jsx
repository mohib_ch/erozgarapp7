import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
  ScrollView,
} from "react-native";
import React from "react";
import { Header } from "../components/header";

const Register = () => {
  return (
    <ScrollView>
      <View style={styles.container}>
        <Header title={"Register"} />
        <View style={styles.form}>
          <TextInput style={styles.input} placeholder="first name" />
          <TextInput style={styles.input} placeholder="last name" />
          <TextInput style={styles.input} placeholder="email" />
          <TextInput style={styles.input} placeholder="password" />
          <TextInput style={styles.input} placeholder="confirm password" />

          <TouchableOpacity style={styles.button}>
            <Text style={styles.buttonText}>Register</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.blank}></View>
      </View>
    </ScrollView>
  );
};

export default Register;

const styles = StyleSheet.create({
  container: {
    flex: 3,
    backgroundColor: "#fff8ff",
  },

  form: {
    flex: 1.5,
    padding: 10,
  },
  input: {
    backgroundColor: "white",
    padding: 15,
    marginVertical: 10,
  },
  button: {
    width: "90%",
    padding: 10,
    backgroundColor: "red",
    borderRadius: 20,
    alignItems: "center",
    alignSelf: "center",
  },
  buttonText: {
    color: "white",
  },
  blank: {
    flex: 1,
  },
});
